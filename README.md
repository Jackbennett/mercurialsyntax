# Mercurial Syntax

Supports line comment/uncomment in commit.hg.txt files.

Couple this with your own commit templates e.g.
```text
[committemplate]
changeset = {desc}\n\n
  HG: # Changed
  HG: *
  HG: # Added
  HG: *
  HG: # Removed
  HG: *
  HG: Enter commit message.  Lines beginning with 'HG:' are removed.
  HG: {extramsg}
  HG: --
  HG: user: {author}\n{ifeq(p2rev, "-1", "",
  "HG: branch merge\n")
  }HG: branch '{branch}'\n{if(activebookmark,
  "HG: bookmark '{activebookmark}'\n")   }{subrepos %
  "HG: subrepo {subrepo}\n"              }{file_adds %
  "HG: added {file}\n"                   }{file_mods %
  "HG: changed {file}\n"                 }{file_dels %
  "HG: removed {file}\n"                 }{if(files, "",
  "HG: no files changed\n")}
```
